<?php
/**
 * @file Configuration page for banners.
 */

/**
 * Render form for global banners settings.
 */
function revive_banners_config_form($form, &$form_state, $no_js_use = FALSE) {
  /*
   * Global display.
   */
  $form['revive_banners_global_display'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display banners on site.'),
    '#default_value' => variable_get('revive_banners_global_display', 1)

  );

  /*
   * Get default zones.
   */
  $def_zones = _revive_banners_default_zones();
  $zones = variable_get('revive_banners_zones', $def_zones);

  // Because we have many fields with the same values, we have to set
  // #tree to be able to access them.
  $form['#tree'] = TRUE;
  $form['names_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Add zones by adding zone name and zone ID'),

    // Set up the wrapper so that AJAX will be able to replace the fieldset.
    '#prefix' => '<div id="names-fieldset-wrapper">',
    '#suffix' => '</div>',
  );

  // Build the fieldset with the proper number of names. We'll use
  // $form_state['num_names'] to determine the number of textfields to build.
  if ($form_state['num_names'] < count($zones)) {
    $form_state['num_names'] = count($zones);
  }
  if (empty($form_state['num_names'])) {
    $form_state['num_names'] = 1;
  }
  for ($i = 0; $i < $form_state['num_names']; $i++) {
    $zone_name = key($zones[$i]);
    $zone_id = $zones[$i][$zone_name];

    $form['names_fieldset']['zone'][$i]['revive_banners_zone_name'][$i] = array(
      '#type' => 'textfield',
      '#title' => t('Zone Name'),
      '#default_value' => isset($zone_name) ? $zone_name : ''
    );
    $form['names_fieldset']['zone'][$i]['revive_banners_zone_id'][$i] = array(
      '#type' => 'textfield',
      '#title' => t('Zone ID'),
      '#default_value' => isset($zone_id) ? $zone_id : ''
    );
  }
  $form['names_fieldset']['add_name'] = array(
    '#type' => 'submit',
    '#value' => t('Add one more'),
    '#submit' => array('revive_banners_add_more_add_one'),

    // See the examples in ajax_example.module for more details on the
    // properties of #ajax.
    '#ajax' => array(
      'callback' => 'revive_banners_add_more_callback',
      'wrapper' => 'names-fieldset-wrapper',
    ),
  );
  if ($form_state['num_names'] > 1) {
    $form['names_fieldset']['remove_name'] = array(
      '#type' => 'submit',
      '#value' => t('Remove one'),
      '#submit' => array('revive_banners_add_more_remove_one'),
      '#ajax' => array(
        'callback' => 'revive_banners_add_more_callback',
        'wrapper' => 'names-fieldset-wrapper',
      ),
    );
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  // This simply allows us to demonstrate no-javascript use without
  // actually turning off javascript in the browser. Removing the #ajax
  // element turns off AJAX behaviors on that element and as a result
  // ajax.js doesn't get loaded.
  // For demonstration only! You don't need this.
  if ($no_js_use) {
    // Remove the #ajax from the above, so ajax.js won't be loaded.
    if (!empty($form['names_fieldset']['remove_name']['#ajax'])) {
      unset($form['names_fieldset']['remove_name']['#ajax']);
    }
    unset($form['names_fieldset']['add_name']['#ajax']);
  }

  return $form;

}

/**
 * Add more add one fields.
 */
function revive_banners_add_more_add_one($form, &$form_state) {
  $form_state['num_names']++;
  $form_state['rebuild'] = TRUE;
}

/**
 * Add more callback.
 */
function revive_banners_add_more_callback($form, $form_state) {
  return $form['names_fieldset'];
}

/**
 * Remove one field.
 */
function revive_banners_add_more_remove_one($form, &$form_state) {
  if ($form_state['num_names'] > 1) {
    $form_state['num_names']--;
  }
  $form_state['rebuild'] = TRUE;
}

/**
 * Submit handler for revive_banners_config_form.
 */
function revive_banners_config_form_submit($form, &$form_state) {
  $zones = array();
  foreach ($form_state['values']['names_fieldset']['zone'] as $zone) {
    $zones[][array_shift($zone['revive_banners_zone_name'])] = array_shift($zone['revive_banners_zone_id']);
  }
  // Set global display.
  $global_display = $form_state['values']['revive_banners_global_display'];
  variable_set('revive_banners_global_display', $global_display);

  // Set zones.
  variable_set('revive_banners_zones', $zones);

  // Set Garfield banner image path.
  variable_set('revive_banners_garfield_image_path', $form_state['values']['revive_banners_garfield_image_path']);

  // Set Garfield banner image path.
  variable_set('revive_banners_zodiac_image_path', $form_state['values']['revive_banners_zodiac_image_path']);

  // Set Weather banner image path.
  variable_set('revive_banners_weather_image_path', $form_state['values']['revive_banners_weather_image_path']);
}
